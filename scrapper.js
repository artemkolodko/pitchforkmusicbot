/*
    Scrapper example taken from here: https://www.codementor.io/johnnyb/how-to-write-a-web-scraper-in-nodejs-du108266t
* */

const cheerio = require("cheerio"),
    req = require("tinyreq");

const constants = require('./constants');

class Scrapper {
    constructor({name, url, title, update = true, onChange = () => {}, period = constants.REQUEST_DATA_PERIOD}) {
        this.name = name;
        this.title = title;
        this.url = url;
        this.update = update;
        this.onChange = onChange;
        this.period = period;
        this.scrappedData = {
            data: [],
            text: ''
        };
        this.start();
    }

    start() {
        this.requestData({});
        if(this.update) {
            this.intervalId = setInterval(this.requestData.bind(this), this.period);
        }
    }

    stop() {
        clearInterval(this.intervalId);
    }

    getText() {
        return this.scrappedData.text;
    }

    onUpdate(newData) {
        let self = this;
        let prevData = this.scrappedData.data;
        let existed = [];
        let appended = newData.filter(item => {
            let existsInOldData = prevData.find(prevItem => {
                /*
                * Strings (after cherio ?) can contain different space symbols (code 160 or 32)
                * */
                return self.isEqualStrings(prevItem.title, item.title);
            });
            if(existsInOldData) {
                existed.push(item);
            }
            return !existsInOldData;
        });

        /*
        if(appended.length > 0 && this.scrappedData.data.length > 0) {
            console.log('old scrapped: ', this.scrappedData.data);
            console.log('\nappended: ', appended);
            console.log('\nexisted: ', existed, '\n\n');
        }
        */

        existed.forEach(item => {
            self.updateExistedItem(item);
        });

        if(appended.length > 0) {
            this.appendItems(appended);
        }
        this.updateText();
        return appended;
    }

    updateExistedItem(newVersion) {
        let self = this;
        this.scrappedData.data = this.scrappedData.data.map(oldVersion => {
            if(!self.isEqualStrings(oldVersion.title, newVersion.title)) {
                return oldVersion;
            }
            return {
                ...newVersion
            };
        })
    }

    appendItems(newItems) {
        if(this.scrappedData.data.length > 0) {
            this.onChange(newItems);
        }
        this.scrappedData.data = newItems.concat(this.scrappedData.data).slice(0, constants.MAX_ROWS_ON_SCREEN);
    }

    updateText() {

    }

    scrape(url, data, cb) {
        // 1. Create the request
        req(url, (err, body) => {
            if (err) { return cb(err); }

            // 2. Parse the HTML
            let $ = cheerio.load(body)
                , pageData = {}
            ;

            // 3. Extract the data
            Object.keys(data).forEach(k => {
                pageData[k] = {};
                pageData[k].el = $(data[k]);
                pageData[k].text = $(data[k]).text();
            });

            // Send the data in the callback
            cb(null, pageData, this);
        });
    }

    isEqualStrings(a, b) {
        let regExp = /[^a-zA-Z]+/g;
        return a.toString().replace(regExp, '') === b.toString().replace(regExp, '');
    }

    requestData() {}
}

module.exports = {
    Scrapper
};