/*
* Best New Albums Page scrapper
* */
const cheerio = require("cheerio");
const Scrapper = require('../scrapper').Scrapper;

class BNAScrapper extends Scrapper {
    constructor({url = 'https://pitchfork.com/reviews/best/albums/', name = 'bna', title='Best New Albums', onChange}) {
        super({url, name, title, onChange})
    }

    attachAlbumScore({title, score}) {
        this.scrappedData.data = this.scrappedData.data.map(album => {
            if(album.title !== title) {
                return album;
            }

            return {
                ...album,
                score: score
            }
        });

        this.updateText();
    }

    updateText() {
        let albums = this.scrappedData.data;
        let text = albums.reduce((previous, current, i) => {
            let score = typeof current.score !== 'undefined' ? '  ['+current.score+']' : '';
            let currentTrackString = (i+1) + '. '+ '<b>'+current.artists + '</b>' + ' - ' +
                '<a href="https://pitchfork.com'+current.link + '">' + current.title + '</a>' +
                score+
                "\n";
            return previous + currentTrackString;
        }, '');

        text = '<a href="'+this.url+'">'+this.title+'</a>\n\n' + text;

        this.scrappedData.text = text;
    }

    requestData() {
        this.scrape(this.url, {
            albums: '.fragment-list .review'
        }, (err, data, scope) => {
            //console.log('Scrapped: ', err || data);

            if(err) {
                return false;
            }

            let newData = data.albums.el.toArray().map(album => {
                let $ = cheerio.load(album);
                //let artists = $(album).find('.album-artist .artist-list li').text();
                let artists = $(album).find('.review__title-artist li').toArray().reduce((prev, next, i, arr) => {
                    let separator = i < arr.length - 1 ? ' / ' : '';
                    return prev + $(next).text() + separator;
                }, '');
                let title = $(album).find('.review__title-album').text();
                let link = $(album).find('.review__link').attr('href');

                // Get album score
                scope.scrape('https://pitchfork.com'+link, {
                    score: '.score-circle .score',
                    title: '.single-album-tombstone__review-title'
                }, (err, data, scope) => {
                    if(err) {
                        return false;
                    }

                    //console.log('Album score', data.score.text, data.title.text);
                    scope.attachAlbumScore({
                        title: data.title.text,
                        score: data.score.text
                    });

                });

                return {
                    artists,
                    title,
                    link
                }
            });

            this.onUpdate(newData);
        });
    }
}

module.exports = {
    BNAScrapper
};