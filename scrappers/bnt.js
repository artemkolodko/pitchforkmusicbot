/*
* Best New Tracks Page scrapper
* */
const cheerio = require("cheerio");
const Scrapper = require('../scrapper').Scrapper;

class BNTScrapper extends Scrapper {
    constructor({
                    url = 'https://pitchfork.com/reviews/best/tracks/?page=1',
                    name = 'bnt',
                    title = 'Best New Track',
                    onChange
    }) {
        super({url, name, title, onChange});
    }

    updateText() {
        let tracks = this.scrappedData.data;
        let text = tracks.reduce((previous, current, i) => {
            let searchString = current.artists.replace(' ', '+')+' - '+current.title.replace(' ', '+');
            let youtubeSearch = '   [<a href="https://www.youtube.com/results?search_query='+ searchString+'">youtube</a>]';
            let currentTrackString = (i+1) +'. '+ '<b>'+current.artists + '</b>' + ' - ' +
                '<a href="https://pitchfork.com'+current.link + '">' + current.title + '</a>'+
                youtubeSearch+
                "\n";
            return previous + currentTrackString;
        }, '');

        text = '<a href="'+this.url+'">'+this.title+'</a>\n\n' + text;
        this.scrappedData.text = text;
    }

    requestData() {
        this.scrape(this.url, {
            title: ".section-nav__menu-item--active",
            trackHeroArtist: '.track-hero .artist-list li',
            trackHeroTrack: '.track-hero .title',
            trackHeroLink: '.track-hero .read-more',
            trackHeroGenre: '.track-hero .genre-list__item a',
            tracks: '.track-list .track-collection-item'
        }, (err, data, scope) => {
            //console.log('Scrapped: ', err || data);

            if(err) {
                return false;
            }

            let tracks = data.tracks.el.toArray().map(track => {
                let $ = cheerio.load(track);
                let artists = $(track).find('.artist-list li').text();
                let title = $(track).find('.track-collection-item__title').text().replace(/["'“”]/g, '');
                let link = $(track).find('.track-collection-item__track-link').attr('href');
                let genre = $(track).find('.genre-list__item a').text();

                return {
                    artists,
                    title,
                    link,
                    genre
                }
            });

            tracks.unshift({
                artists: data.trackHeroArtist.text,
                title: data.trackHeroTrack.text.replace(/["'“”]/g, ''),
                link: data.trackHeroLink.el.attr('href'),
                genre: data.trackHeroGenre.text,
                hero: true
            });

            this.onUpdate(tracks);
        });
    }
}

module.exports = {
    BNTScrapper
};

