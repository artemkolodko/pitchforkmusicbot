/*
* Best Songs scrapper
* */
const cheerio = require("cheerio");
const Scrapper = require('../scrapper').Scrapper;

class BestSongs extends Scrapper {
    constructor({
                    url = 'https://pitchfork.com/features/lists-and-guides/the-100-best-songs-of-2017/?page=10',
                    name = 'bs2017',
                    title = 'The Best Songs of 2017',
                    update = false
    }) {
        super({url, name, title, update})
    }

    updateText() {
        let tracks = this.scrappedData.data;
        let text = tracks.reduce((previous, current, i) => {

            let searchString = current.artists.replace(' ', '+')+' - '+current.title.replace(' ', '+');
            let youtube = '   [<a href="https://www.youtube.com/results?search_query='+ searchString+'">youtube</a>]';

            let currentTrackString = (i+1) +'. '+ '<b>'+current.artists + '</b>' + ' - ' + current.title +
                youtube+
                "\n";
            return previous + currentTrackString;
        }, '');

        text = '<a href="'+this.url+'">'+this.title+'</a>\n\n' + text;
        this.scrappedData.text = text;
    }

    requestData() {
        this.scrape(this.url, {
            tracks: '.list-blurb__tombstone'
        }, (err, data) => {
            //console.log('Scrapped: ', err || data);

            if(err) {
                return false;
            }

            let tracks = data.tracks.el.toArray().map(track => {
                let $ = cheerio.load(track);
                let artists = $(track).find('.artist-list li').text();
                let title = $(track).find('.list-blurb__work-title').text().replace(/["'“”]/g, '');
                return {artists, title}
            });

            tracks.reverse();

            this.onUpdate(tracks);
        });
    }
}

module.exports = {
    BestSongs
};