/*
* Best Songs scrapper
* */
const cheerio = require("cheerio");
const Scrapper = require('../scrapper').Scrapper;

class News extends Scrapper {
    constructor({url = 'https://pitchfork.com/news/?page=1', title='News', name='news', onChange}) {
        super({url, title, name, onChange})
    }

    updateText() {
        let news = this.scrappedData.data;
        let text = news.reduce((previous, current, i) => {
            let article = '<a href="https://pitchfork.com' + current.link + '">' + current.title + '</a>' + ' - ' +
                ' ['+current.pubDate+']'+
                "\n";
            return previous + article;
        }, '');
        text = '<a href="'+this.url+'">'+this.title+'</a>\n\n' + text;
        this.scrappedData.text = text;
    }

    requestData() {
        this.scrape(this.url, {
            news: '.article-details'
        }, (err, data, scope) => {
            //console.log('Scrapped: ', err || data);

            if(err) {
                return false;
            }

            let news = data.news.el.toArray().map(article => {
                let $ = cheerio.load(article);
                let title = $(article).find('.title').text();
                let link = $(article).find('.title-link').attr('href');
                let pubDate = $(article).find('.pub-date ').text();
                //console.log(title, link, pubDate);
                return {title, link, pubDate}
            });

            this.onUpdate(news);
        });
    }
}

module.exports = {
    News
};