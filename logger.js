let fs = require('fs');

const LOG_DIRECTORY = 'logs/';
const SUBSCRIBERS_FILENAME = 'subscribers.txt';
const PATH_TO_SUBSCRIBERS = LOG_DIRECTORY + SUBSCRIBERS_FILENAME;

class Logger {
    constructor() {}

    appendChatId(chatId) {
        fs.appendFile(PATH_TO_SUBSCRIBERS, chatId + ',', function (err) {
            if (err) throw err;
            //console.log('Saved!', chatId);
        });
    }

    writeSubscribersList(subscribers) {
        let self = this;
        let subscriberIds = subscribers.reduce((prev, next) => {
                return prev + next + ',';
        }, '');

        fs.writeFile(PATH_TO_SUBSCRIBERS, subscriberIds, function (err) {
            if (err) throw err;
        });
    }

    // sync(blocking) file read is OK because its caused only once on bot initialization
    getSubscribersFromFile() {
        let contents = fs.readFileSync(PATH_TO_SUBSCRIBERS, 'utf8');
        return contents.split(',').filter(id => id !== '').map(id => parseInt(id));
    }
}

module.exports = {
    Logger
};