const messages = {
    UNKNOWN: 'Unrecognized command. Say what? \n/help',
};

module.exports = {
    messages
};

