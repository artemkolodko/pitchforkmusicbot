class Menu {
    constructor() {}

    getText(isSubscriber) {
        let subscribeOption = isSubscriber ?
            '/unsubscribe - Unsubscribe from updates (News, Best New Tracks ans latest album reviews)':
            '/subscribe - Subscribe to updates (News, Best New Tracks ans latest album reviews)';
        return 'You can control me by sending these commands:\n' +
            '\n' +
            '<b>Latest</b>\n'+
            '/news - News\n' +
            '\n' +
            '<b>Reviews</b>\n'+
            '/all - All Reviews\n' +
            '/high_scoring - 8.0+ Reviews\n' +
            '/rap - By genre (Rap)\n' +
            '/pop - By genre (Pop/R&B)\n' +
            '/electronic - By genre (Electronic)\n' +
            '/metal - By genre (Metal)\n' +
            '\n' +
            '<b>Best New Music</b>\n'+
            '/bnt - Best New Tracks\n' +
            '/bna - Best New Albums\n' +
            '/bnr - Best New Reissues\n' +
            '\n'+
            '<b>Features & Lists</b>\n'+
            '/ba2017 - Best Albums of 2017\n' +
            '/bs2017 - Best Songs of 2017\n' +
            '/bs2016 - Best Songs of 2016\n' +
            '/bs2015 - Best Songs of 2015\n' +
            '\n'+
            //'<b>Settings</b>\n'+
            //subscribeOption +
            '\n\n';
    }
}

module.exports = {
    Menu
};