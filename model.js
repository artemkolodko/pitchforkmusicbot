const TelegramBot = require('node-telegram-bot-api');
const Agent = require('socks5-https-client/lib/Agent')

const constants = require('./constants');
const Logger = require('./logger.js').Logger;
const Menu = require('./menu.js').Menu;
const messages = require('./messages').messages;
const BNTScrapper = require('./scrappers/bnt.js').BNTScrapper;
const BNAScrapper = require('./scrappers/bna.js').BNAScrapper;
const BestSongs = require('./scrappers/best_songs.js').BestSongs;
const News = require('./scrappers/news.js').News;

class BotModel {
    constructor({token}) {
        const self = this;
        this.listener = new TelegramBot(token, {
            polling: true,
            request: {
                agentClass: Agent,
                agentOptions: {
                    socksHost: 'srv2.freetelegram.org',
                    socksPort: 10000,
                    socksUsername: 'freetelegram.org',
                    socksPassword: 'freetelegram.org'
                }
            }
        });

        this.listener.on('callback_query', (query) => {
            const text = query.data;
            const chatId = query.from.id;
            let answer = self.onMessage(text, chatId);
            self.sendMessage(chatId, answer);
        });

        this.listener.on('message', (msg) => {
            const text = msg.text;
            const chatId = msg.chat.id;
            let answer = self.onMessage(text, chatId);
            self.sendMessage(chatId, answer);
        });

        this.logger = new Logger();
        this.subscribers = this.logger.getSubscribersFromFile();
        this.subscribersChanged = false;
        this.menu = new Menu();
        this.initScrappers();

        /*
        setInterval(() => {
            if(self.subscribersChanged) {
                self.logger.writeSubscribersList(self.subscribers);
                self.subscribersChanged = false;
            }
        }, constants.SYNC_SUBSCRIBERS_INTERVAL)
        */
    }

    sendMessage(chatId, msg) {
        this.listener.sendMessage(chatId, msg, {
            parse_mode: 'HTML',
            reply_markup: {
                inline_keyboard: [
                    [{text: 'Menu', callback_data: '/menu'}]
                ]
            }
        });
    }

    onMessage(msgText, chatId) {
        let answer = '';
        switch(msgText) {
            case '/news': answer = this.news.getText(); break;
            case '/all': answer = this.all.getText(); break;
            case '/high_scoring': answer = this.high_scoring.getText(); break;
            case '/rap': answer = this.rap.getText(); break;
            case '/electronic': answer = this.electronic.getText(); break;
            case '/pop': answer = this.pop.getText(); break;
            case '/metal': answer = this.metal.getText(); break;
            case '/bnt': answer = this.bnt.getText(); break;
            case '/bna': answer = this.bna.getText(); break;
            case '/bnr': answer = this.bnr.getText(); break;
            case '/ba2017': answer = this.ba2017.getText(); break;
            case '/bs2017': answer = this.bs2017.getText(); break;
            case '/bs2016': answer = this.bs2016.getText(); break;
            case '/bs2015': answer = this.bs2015.getText(); break;
            case '/subscribe':
                this.subscribe(chatId);
                answer = 'You successfully subscribed to updates! \n';
                break;
            case '/unsubscribe':
                if(this.unSubscribe(chatId)) {
                    answer = 'You successfully unsubscribed from updates! \n';
                } else {
                    answer = 'Error occured \n\n/menu';
                }

                break;
            case '/start': case '/help': case '/menu':
                answer = this.getMenu(chatId); break;
            default:
                answer = messages.UNKNOWN;
        }

        return answer;
    }

    isSubscriber(chatId) {
        return this.subscribers.indexOf(chatId) !== -1;
    }

    addChatId(chatId) {
        this.subscribers.push(chatId);
    }

    notifyAllSubscribers(text) {
        let self = this;

        //console.log('Notification: ', text);

        this.subscribers.forEach(chatId => {
            self.sendMessage(chatId, text);
        })
    }

    subscribe(chatId) {
        if (this.subscribers.indexOf(chatId) === -1) {
            this.addChatId(chatId);
            this.subscribersChanged = true;
            return true;
        }
        return false;
    }

    unSubscribe(chatId) {
        let chatIdIndex = this.subscribers.indexOf(chatId);
        if (chatIdIndex !== -1) {
            this.subscribers.splice(chatIdIndex, 1);
            this.subscribersChanged = true;
            return true;
        }
        return false;
    }

    getMenu(chatId) {
        let isSubscriber = this.isSubscriber(chatId);
        return this.menu.getText(isSubscriber);
    }

    initScrappers() {
        let self = this;
        this.bnt = new BNTScrapper({});

        this.bna = new BNAScrapper({});

        this.all = new BNAScrapper({
            url: 'https://pitchfork.com/reviews/albums/?page=1',
            title: 'All reviews'
        });

        this.news = new News({});

        this.metal = new BNAScrapper({
            url: 'https://pitchfork.com/reviews/albums/?genre=metal',
            title: 'Latest Reviews by genre: Metal'
        });

        this.rap = new BNAScrapper({
            url: 'https://pitchfork.com/reviews/albums/?genre=rap',
            title: 'Latest Reviews by genre: Rap'
        });

        this.electronic = new BNAScrapper({
            url: 'https://pitchfork.com/reviews/albums/?genre=electronic',
            title: 'Latest Reviews by genre: Electronic'
        });

        this.pop = new BNAScrapper({
            url: 'https://pitchfork.com/reviews/albums/?genre=pop',
            title: 'Latest Reviews by genre: Pop/R&B'
        });

        this.bnr = new BNAScrapper({
            name: 'bnr',
            title: 'Best New Reissue',
            url: 'https://pitchfork.com/reviews/best/reissues/?page=1'
        });
        this.high_scoring = new BNAScrapper({
            name: 'high_scoring',
            title: '8.0+ Reviews',
            url: 'https://pitchfork.com/best/high-scoring-albums/?page=1'
        });

        this.ba2017 = new BestSongs({
            url: 'https://pitchfork.com/features/lists-and-guides/the-50-best-albums-of-2017/?page=5',
            title: 'Best Albums of 2017'
        });

        this.bs2017 = new BestSongs({});

        this.bs2016 = new BestSongs({
            url: 'https://pitchfork.com/features/lists-and-guides/9981-the-100-best-songs-of-2016/?page=10',
            title: 'Best Songs of 2016'
        });

        this.bs2015 = new BestSongs({
            url: 'https://pitchfork.com/features/lists-and-guides/9765-the-100-best-tracks-of-2015/',
            title: 'Best Songs of 2015'
        });
    }
}

module.exports = {
    BotModel
};